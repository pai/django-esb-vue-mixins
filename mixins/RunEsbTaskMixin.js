/**
 * @mixin
 */


export default {
  props: {
    esbTaskPollingTimeout: {
      type: [String, Number],
      default: 1999
    },
    esbTaskSuccessStates: {
      type: Array,
      default () {
        return ['PENDING', 'RECEIVED', 'STARTED', 'RETRY']
      }
    },
    esbTaskErrorStates: {
      type: Array,
      default () {
        return ['FAILURE', 'REVOKED']
      }
    }
  },
  methods: {
    watchEsbTask (url) {
      //console.log('watchEsbTask', url)
      this.$http.get(url,
        {
          withCredentials: true
        }
      ).then((res) => {
        let status = res.data.status
        //console.log('pending status', status)

        if (this.esbTaskSuccessStates.indexOf(status) > -1) { // pending status
          setTimeout(() => {
            this.watchEsbTask(url)
          }, this.esbTaskPollingTimeout)
        } else if (status === 'SUCCESS'){ // success status
          this.$emit('runEsbTaskSuccess', res.data)
        } else if (this.esbTaskErrorStates.indexOf(status) > -1){ // error status
          this.$emit('runEsbTaskError', 'Error running tasks. RPC failed.')
        } else {
          this.$emit('runEsbTaskError', 'Error running tasks. Wrong status' + '(' + status + ').')
        }
      }).catch((err) => {
        //console.log('watchEsbTask error', err)
        this.$emit('runEsbTaskError', err)
      })
    },
    runEsbTask(url, payload) {
      // run single task
      //console.log('runEsbTask', url, payload)

      this.$emit('runningEsbTask')
      this.$http.post(url,
        payload,
        {
          withCredentials: true
        }
      ).then((res) => {
        this.$emit('esbTaskStarted', res.data)
        let taskUrl = res.data.url

        this.watchEsbTask(taskUrl)
      }).catch((err) => {
        this.$emit('runEsbTaskError', err)
      })
    },
    runEsbTaskP (url, payload) {
      return new Promise((resolve, reject) => {
        this.$on('runEsbTaskSuccess', (response) => {
          resolve(response)
        })
        this.$on('runEsbTaskError', (error) => {
          reject(error)
        })

        this.runEsbTask(url, payload)
      })
    }
  }
}
